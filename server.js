
var http        = require('http'),
	fs          = require('fs'),
	express     = require('express'),
	querystring = require('querystring'),
	app         = express();

app.use(express.logger('dev'));

app.use(express.static(__dirname ));

app.listen(process.env.PORT || 8080);

